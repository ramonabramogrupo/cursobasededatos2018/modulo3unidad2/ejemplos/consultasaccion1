﻿/* 
  consultas de accion 1
*/

/*
  ejercicio 1
*/

  -- trabajo con bases de datos
  DROP DATABASE IF EXISTS consultasAccion1;
  CREATE DATABASE IF NOT EXISTS consultasAccion1;
  USE consultasAccion1;

  -- tabla productos
  CREATE TABLE IF NOT EXISTS productos(
      id int AUTO_INCREMENT,
      nombre varchar(50),
      precio float,
      peso int,
      grupo enum('g1','g2','g3'),
      cantidad int,
      PRIMARY KEY(id)
    )ENGINE INNODB;

  -- tabla cliente
CREATE TABLE IF NOT EXISTS cliente(
    id int AUTO_INCREMENT,
    nombre varchar(50),
    apellidos varchar(50),
    nombreCompleto varchar(120),
    cantidad int,
    descuento float,
    PRIMARY KEY (id)
  )ENGINE INNODB;

-- tabla venden
CREATE TABLE IF NOT EXISTS venden(
  idProducto int,
  idCliente int,
  fecha date DEFAULT '2010/1/1', 
  cantidad int NOT NULL DEFAULT 0,
  total float,
  PRIMARY KEY (idProducto,idCliente)
  )ENGINE INNODB;

-- creamos las claves ajenas
ALTER TABLE venden 
  -- primera clave ajena
  ADD CONSTRAINT FKvendenClientes FOREIGN KEY (idCliente)
  REFERENCES cliente(id) ON DELETE CASCADE ON UPDATE CASCADE,
  -- segunda clave ajena
  ADD CONSTRAINT FKvendenProductos FOREIGN KEY (idProducto)
  REFERENCES productos(id) ON DELETE CASCADE ON UPDATE CASCADE;


/*
  Ejercicio 2
  Fichero datosgenerados.sql
*/


  /* 
  ejercicio 3
  */

  UPDATE productos p 
    SET cantidad=NULL;
  
  UPDATE cliente c
    SET cantidad=NULL;
  
  UPDATE cliente c
    SET c.nombreCompleto=NULL;

  UPDATE venden 
    SET total=NULL;

  /* 
  EJERCICIO 4A
  */
  
  -- consulta de seleccion
  SELECT p.precio*v.cantidad FROM venden v JOIN productos p 
    ON p.id=v.idProducto;

  -- accion
  UPDATE venden v JOIN productos p ON p.id=v.idProducto
    SET v.total=p.precio*v.cantidad;

  /* 
  EJERCICIO 4B
  */

  -- seleccion
  SELECT CONCAT_WS('-',c.nombre,c.apellidos) FROM cliente c;

  -- accion
  UPDATE cliente c 
    SET c.nombreCompleto=CONCAT_WS('-',c.nombre,c.apellidos);

/* 
  ejercicio 5A
*/
  
  -- consulta de seleccion
  SELECT p.precio*v.cantidad*(1-c.descuento/100)
    FROM productos p JOIN venden v ON p.id=v.idProducto
    JOIN cliente c ON c.id=v.idCliente;

  -- consulta de accion 
    UPDATE productos p JOIN venden v ON p.id=v.idProducto
      JOIN cliente c ON c.id=v.idCliente
    SET v.total=p.precio*v.cantidad*(1-c.descuento/100);

    /*
      Ejercicio 5B
    */

    -- seleccion
    CREATE OR REPLACE VIEW productosVendidos AS 
      SELECT v.idProducto,SUM(cantidad) total 
        FROM venden v GROUP BY v.idProducto;

    -- consulta de accion
    UPDATE productos p JOIN productosVendidos pv 
      ON p.id=pv.idProducto
      SET p.cantidad=pv.total;

  /*
      Ejercicio 5C
  */

    -- seleccion
    CREATE OR REPLACE VIEW productosPorCliente as
    SELECT 
      v.idCliente,SUM(v.cantidad) total
    FROM venden v GROUP BY v.idCliente;

    -- accion
    UPDATE cliente c JOIN productosPorCliente pc 
      ON c.id=pc.idCliente
      SET c.cantidad=pc.total;

    /* 
      Ejercicio 6
    */
      ALTER TABLE productos
        ADD COLUMN precioTotal float;
      
      ALTER TABLE cliente
        ADD COLUMN precioTotal float;

/*
  Ejercicio 7A
*/

  -- seleccion
  CREATE OR REPLACE VIEW totalProducto AS 
  SELECT v.idProducto,SUM(v.total) total
    FROM venden v GROUP BY v.idProducto;

  -- accion
  UPDATE productos p JOIN totalProducto tp 
    ON p.id=tp.idProducto
    SET p.precioTotal=tp.total;

/*
  Ejercicio 7B
*/

  -- seleccion
  CREATE OR REPLACE VIEW totalCliente AS 
  SELECT v.idCliente,SUM(v.total) total
    FROM venden v GROUP BY v.idCliente;

  -- accion
  UPDATE cliente c JOIN totalCliente tc 
    ON c.id=tc.idCliente
    SET c.precioTotal=tc.total;





    























